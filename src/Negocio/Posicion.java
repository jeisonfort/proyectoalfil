/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author JEISON_FORT,GUILLERMO_GU
 */
public class Posicion {
    private int fila;
    private int columna;
    
    public Posicion(int fila,int columna){
        this.fila=fila;
        this.columna=columna;
    }

    public int getF() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getC() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
    
    public boolean equals(Posicion otra){
        return (this.fila==otra.getF())&&(this.columna==otra.getC());
    }
}
