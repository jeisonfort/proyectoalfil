/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Ficha;
import ufps.util.colecciones_seed.Pila;
/**
 *
 * @author JEISON_FORT,GUILLERMO_GU
 */
public class Tablero {
    
    private boolean dirPeon;
    
    //private Posicion peon;
    //private Posicion alfil;
    private Ficha [][]myTablero;
    //Adicione los atributos que considere necesarios para el correcto fucnioanmiento de su aplicación, si y solo si , no violen ninguna regla en POO
    public Tablero() {
    }
    
    /**
     * Constructor para iniciar el juego
     * @param i_alfil Posición de la fila para el alfil
     * @param j_alfil Posición de la columna para el alfil
     * @param i_peon  Posición de la fila para el peon
     * @param j_peon  Posición de la columna para el alfil
     * @param dirPeon true si el peón se mueve de arriba hacia abajo, o false en caso contrario
     */   
     public Tablero(int i_alfil, int j_alfil, int i_peon,int j_peon, boolean dirPeon) {
                  
         this.dirPeon=dirPeon;
         this.myTablero= new Ficha[8][8];
         this.myTablero[i_alfil][j_alfil]= new Ficha("A");
         this.myTablero[i_peon][j_peon]=new Ficha("P");
         
       
         
    } 
     /**
      * Retorna la dirección del movimiento.
      * @return True si es hacia abajo. False si es hacia arriba.
      */
    public boolean isDirPeon() {
        return dirPeon;
    }
    /**
     * Cambia la dirección del movimiento del PEON.
     * @param dirPeon 
     */
    public void setDirPeon(boolean dirPeon) {
        this.dirPeon = dirPeon;
    }
    /**
     * Busca todos las fichas de la matriz marcadas con 'a' (los cruces del alfil-peon) y las vuelve '-'
     * Evita que se den errores cuando el alfil marque casillas como visitadas. 
     */
    private void limpiarCruces(){
        for (Ficha[] myTablero1 : this.myTablero) {
            for (Ficha myTablero11 : myTablero1) {
                if (myTablero11 != null && myTablero11.getNombreFicha().equals("a")) {
                    myTablero11.setNombreFicha("-");
                }
            }
        }
    }
   /**
    * Calcula y marca con "a" las casillas donde se cruce el alfil con el peon.
    * @param filaA
    * @param columnaA
    * @param columnaP 
    * @return Retorna la cantidad de cruces que interfieran en el camino del peon. 
    */
    private int buscarCruceArriba(int filaA, int columnaA, int columnaP,int filaP){
        int d = Math.abs(columnaP - columnaA);  
        int c=0;   
        limpiarCruces();
        if(columnaA==columnaP && filaA<filaP){
            this.myTablero[filaA][columnaA]=new Ficha("a");
            return 1;
        }
            
        if(d!=0){
            //CRUCE ABAJO
            if(filaA+d >=0 &&filaA+d <8 && filaP>filaA+d){
                this.myTablero[filaA+d][columnaP]=new Ficha("a");
                c++;
            }
            //CRUCE ARRIBA
            if(filaA-d >=0 &&filaA-d <8 && filaP>filaA-d){
                 this.myTablero[filaA-d][columnaP]=new Ficha("a");         
                c++;
            }
            
        }    
        return c;
    }
    /**
    * Calcula y marca con "a" las casillas donde se cruce el alfil con el peon.
    * @param filaA
    * @param columnaA
    * @param columnaP 
    * @return Retorna la cantidad de cruces que interfieran en el camino del peon. 
    */
    private int buscarCruceAbajo(int filaA, int columnaA, int columnaP,int filaP){
        int d = Math.abs(columnaP - columnaA);  
        int c=0;   
        limpiarCruces();
        //El Alfil ta al frente del PEON
        if(columnaA==columnaP && filaA>filaP){
            this.myTablero[filaA][columnaA]=new Ficha("a");
            return 1;
        }
            
        if(d!=0){
            //CRUCE ARRIBA
            if(filaA+d >=0 &&filaA+d <8 && filaP<filaA+d){
                this.myTablero[filaA+d][columnaP]=new Ficha("a");
                c++;
            }
            //CRUCE ABAJO
            if(filaA-d >=0 &&filaA-d <8 && filaP<filaA-d){
                 this.myTablero[filaA-d][columnaP]=new Ficha("a");         
                c++;
            }
            
        }    
        return c;
    }
    /**
     * Busca el cruce del alfil con el peon y lo marca con una 'a'
     * @param filaA
     * @param columnaA
     * @param columnaP
     * @param filaP
     * @return returna la cantidad de cruces que se encuentren en el camino del peon.
     */
    private int buscarCruce(int filaA, int columnaA, int columnaP,int filaP){
        if(this.dirPeon){
            return buscarCruceAbajo(filaA, columnaA, columnaP, filaP);
        }
        else 
            return buscarCruceArriba(filaA, columnaA, columnaP, filaP);
    }
   /**
    * Calcula la cantidad de casillas donde se cruce el alfil con el peon.
    * @param filaA
    * @param columnaA
    * @param columnaP 
    * @return Retorna la cantidad de cruces que interfieran en el camino del peon. 
    */
    private int buscarCruceAlfilArriba(int filaA, int columnaA, int columnaP,int filaP){
        int d = Math.abs(columnaP - columnaA);  
        int c=0;   
        limpiarCruces();
        if(columnaA==columnaP && filaA<filaP)
            return 1;
        if(d!=0){
            //CRUCE ABAJO
            if(filaA+d >=0 &&filaA+d <8 && filaP>filaA+d){
                c++;
            }
            //CRUCE ARRIBA
            if(filaA-d >=0 &&filaA-d <8 && filaP>filaA-d){                
                c++;
            }
            
        }    
        return c;
    }
    /**
    * Calcula la cantidad de casillas donde se cruce el alfil con el peon.
    * @param filaA
    * @param columnaA
    * @param columnaP 
    * @return Retorna la cantidad de cruces que interfieran en el camino del peon. 
    */
    private int buscarCruceAlfilAbajo(int filaA, int columnaA, int columnaP,int filaP){
        int d = Math.abs(columnaP - columnaA);  
        int c=0;   
        limpiarCruces();
        //El Alfil ta al frente del PEON
        if(columnaA==columnaP && filaA>filaP){        
            return 1;
        }
            
        if(d!=0){
            //CRUCE ARRIBA
            if(filaA+d >=0 &&filaA+d <8 && filaP<filaA+d){     
                c++;
            }
            //CRUCE ABAJO
            if(filaA-d >=0 &&filaA-d <8 && filaP<filaA-d){      
                c++;
            }
            
        }    
        return c;
    }
    public int buscarCruceAlfil(int filaA, int columnaA, int columnaP,int filaP){
        if(this.dirPeon){
            return buscarCruceAlfilAbajo(filaA, columnaA, columnaP, filaP);
        }
        else 
            return buscarCruceAlfilArriba(filaA, columnaA, columnaP, filaP);
    
    }
    /**
     * Método que mueve el PEON cuando el movimiento es hacia arriba.
     * @param filaP
     * @param columnaP
     * @param pila
     * @return Retorna la FILA donde terminó el PEON despues de moverse. 
     */
    public int jugarPeonArriba(int filaP, int columnaP,Pila<String> pila){
       
        
        if(filaP==0){  
            this.myTablero[filaP][columnaP]=new Ficha("P");
            //pila.apilar("P("+filaP+","+columnaP+") ");
            return 0;
        }
        if(filaP>0 && this.myTablero[filaP][columnaP]!=null
                &&this.myTablero[filaP][columnaP]
                .getNombreFicha().equals("a")){
            this.myTablero[filaP][columnaP]=new Ficha("-");
            this.myTablero[filaP+1][columnaP]=new Ficha("P");
            pila.apilar("P("+(filaP+1)+","+columnaP+") ");
            return filaP+1;
            
        }
        
        this.myTablero[filaP][columnaP]=new Ficha("-");
        return jugarPeonArriba(--filaP,columnaP,pila);
       
    }
    /**
     * Método que mueve el PEON cuando el movimiento es hacia abajo.
     * @param filaP
     * @param columnaP
     * @param pila
     * @return Retorna la FILA donde terminó el PEON despues de moverse. 
     */
    public int jugarPeonAbajo(int filaP, int columnaP,Pila<String> pila){
       
      
        if(filaP==7){  
            this.myTablero[filaP][columnaP]=new Ficha("P");
            //pila.apilar("P("+filaP+","+columnaP+") ");
            return 7;
        }
        //Si encuentra una 'a' es un cruce con el alfil. 
        if(filaP<7 && this.myTablero[filaP][columnaP]!=null
                &&this.myTablero[filaP][columnaP]
                .getNombreFicha().equals("a")){
            this.myTablero[filaP][columnaP]=new Ficha("-");
            this.myTablero[filaP-1][columnaP]=new Ficha("P");
            pila.apilar("P("+(filaP-1)+","+columnaP+") ");
            
            return filaP-1;
            
        }
        
        
        imprimirTablero();
        this.myTablero[filaP][columnaP]=new Ficha("-");
        return jugarPeonAbajo(++filaP,columnaP,pila);
       
    }
    /**
     * Metodo que escoge que instrucción debe ejecutarse dependiendo de hacia donde valla el movimiento.
     * @param filaP
     * @param columnaP
     * @param pila
     * @return La Fila donde se encuentra al PEON despues de moverse. 
     */
    public int jugarPeon(int filaP, int columnaP,Pila<String> pila){
    
        if(this.dirPeon){
            return jugarPeonAbajo(filaP, columnaP, pila);
        }
        else{
            return jugarPeonArriba(filaP, columnaP, pila);
        }
    }
    

    /**
     * Método que recorre la matriz y busca las posiciones actuales del ALFIL y el PEON.
     * @param arr 
     */
    private void recorrer(int arr[]){
        
        for(int i=0;i<this.myTablero.length;i++){
            for(int j = 0; j<this.myTablero[i].length;j++){
                if(this.myTablero[i][j]!=null){
                    switch (this.myTablero[i][j].getNombreFicha()) {
                        case "A":
                            arr[0]=i;
                            arr[1]=j;
                            break;
                        case "P":
                            arr[2]=i;
                            arr[3]=j;
                            break;
                        case "a":
                            this.myTablero[i][j]=new Ficha("-");
                            break;
                        default:
                            break;
                    }
                    
                }
            }
        }
    }
    /**
     * Método que imprime el tablero en consola.
     */
    public void imprimirTablero(){
        for (Ficha[] myTablero1 : myTablero) {
            for (Ficha myTablero11 : myTablero1) {
                if (myTablero11 == null) {
                    System.out.print("- ");
                } else {
                    System.out.print(myTablero11.getNombreFicha() + " ");
                }
            }
            System.out.println("");
        }
        System.out.println("----------------------------------");
    }
    
    
   
   /**
    * Recibe las PILAS y retorna un String con los movientos en orden.
    * @param pila
    * @param pilaA
    * @param cP
    * @return 
    */
    private String mostrarMovimientos(Pila<String> pila, Pila<String>pilaA, int cP){
       String msg = "";
        
       while(!pilaA.esVacia()){          
            pila.apilar(pilaA.desapilar());
        
        }
        while(!pila.esVacia()){
            msg=pila.desapilar()+msg;
        }
        int r=0;
        int i=7;
        if(this.dirPeon){
            r=7;
            i=0;
        }
        return "P("+i+","+cP+") "+msg+"P("+r+","+cP+") ";
    }
    /**
     * Marca el la matriz los movimientos del ALFIL enumerados en orden.
     * @param posiciones 
     */
    public void marcarMovimientos(Pila<Posicion> posiciones){
        Posicion pos = new Posicion(0, 0);
        int i=0;
        while(!posiciones.esVacia()){
            pos= posiciones.desapilar();
            this.myTablero[pos.getF()][pos.getC()]=new Ficha("A"+i);
            i++;
        }
    }
    /**
     * Método principal para el movimiento del ALFIL.
     * Movera el ALFIL hasta que encuentre una ruta hacia una posición que no tenga cruces con el movimiento del peon
     * @param sol
     * @param pos
     * @param columnaP
     * @param filaP
     * @param pila
     * @return Retorna una PILA con los movimientos del ALFIL. 
     */
    public Pila<Posicion> jugarAlfil(Pila<Posicion> sol, int pos[], int columnaP, int filaP,Pila<String>pila) {
        //1. Pregunte cruces: Si no hay GANO (RETURN)- SI hay VALIDAR que estén adelante. 
        //2. Marcar la posición.
        //3. Si se puede mover abajo izq. BAJE y RETURN jugarAlfil()
        //4. Si se puede mover abajo der. BAJE Y RETURN jugarAlfil()
        //5. Si se puede mover ARRIBA IZQ. Suba y return jugarAlfil()
        //6. Si se puede mover ARRIBA DER. Suba y return jugarAlfil()
        //7. Si no encontró ninguno, analizar mejor movimiento. Y return para esperar mov de PEON
        //8. Si encuentra regresarse e ir guardando las pos.
        System.out.println("TABLERO ALFIL");
        System.out.println("POS: " + pos[0] + " - " + pos[1]);
        imprimirTablero();

        Posicion act = new Posicion(pos[0], pos[1]);
        //Comprobar que la casilla no ha sido RECORRIDA.
        if (this.myTablero[pos[0]][pos[1]] != null
                && this.myTablero[pos[0]][pos[1]].getNombreFicha().equals("V")) {
            return sol;

        }
        this.myTablero[pos[0]][pos[1]] = new Ficha("V");
        int buscarCruce = buscarCruceAlfil(pos[0], pos[1], columnaP, filaP);
        if (buscarCruce == 0) {
            sol.apilar(act);
           pila.apilar("A("+act.getF()+","+act.getC()+") ");
            return sol;
        }
        //Ir abajo
        if (pos[0] < 7) {
            //SI Puedo ir abajo a la izquierda
            if (pos[1] > 0 && sol.esVacia()) {
                pos[0]++;
                pos[1]--;
                jugarAlfil(sol, pos, columnaP, filaP,pila);
                pos[0]--;
                pos[1]++;
            }
            
            if (pos[1] < 7 && sol.esVacia()) {
                pos[0]++;
                pos[1]++;
                jugarAlfil(sol, pos, columnaP, filaP,pila);
                pos[0]--;
                pos[1]--;
            }
        }
        //Ir arriba
        if (pos[0] > 0) {
            //SI Puedo ir ARRIBA a la izquierda
            if (pos[1] > 0 && sol.esVacia()) {
                pos[0]--;
                pos[1]--;
                jugarAlfil(sol, pos, columnaP, filaP,pila);
                pos[0]++;
                pos[1]++;
            }
            //Arriba a la derecha
            
            if (pos[1] < 7 && sol.esVacia()) {
                pos[0]--;
                pos[1]++;
                jugarAlfil(sol, pos, columnaP, filaP,pila);
                pos[0]++;
                pos[1]--;
            }

        }

        if (!sol.esVacia()) {
            sol.apilar(act);
            pila.apilar("A("+act.getF()+","+act.getC()+") ");
           
        }
          return sol;  
        //return jugarAlfil(sol, pos, columnaP, filaP,pila); //NO deberia o si?
    }
    /**
     * Recorre el tablero buscando las 'V' que marcan las casillas como visitadas y las reinicia.
     */
    private void limpiarVisitados(){
        for (Ficha[] myTablero1 : this.myTablero) {
            for (Ficha myTablero11 : myTablero1) {
                if (myTablero11 != null && myTablero11.getNombreFicha().equals("V")) {
                    myTablero11.setNombreFicha("-");
                }
            }
        }
    }
    /**
      * Este método movera el alfil cuando el método jugarAlfil() no encuentre ningúna solución. 
      * @param pos
      * @param cp
      * @param pila 
      */
    public void moverAlfilAuxArriba(int pos[],int cp, Pila<String> pila){
        //1.Moverse lejo del peon.
        //2.Moverse hacia abajo. 
        //3 Moverse lejos hacia arriba si se puede
        //4.Moverse hacia el peon y arriba.
        
        limpiarVisitados();
        int d = cp-pos[1];
        //Mover siempre hacia abajo si se puede.
        if(pos[0]<7 && pos[1]!=0 &&pos[1]!=7){
            pos[0]++;
        }
        else{
            pos[0]--;
        }
        if(d>0){
            if(pos[1]>0){
                pos[1]--;
            }
            else
                pos[1]++;
        }
        else{
            if(pos[1]<7){
                pos[1]++;
            }
            else
                pos[1]--;
        }
        pila.apilar("A("+pos[0]+","+pos[1]+") ");
    }
    /**
      * Este método movera el alfil cuando el método jugarAlfil() no encuentre ningúna solución. 
      * @param pos
      * @param cp
      * @param pila 
      */
     public void moverAlfilAuxAbajo(int pos[],int cp, Pila<String> pila){
        //1.Moverse lejo del peon.
        //2.Moverse hacia abajo. 
        //3 Moverse lejos hacia arriba si se puede
        //4.Moverse hacia el peon y arriba.
        
        limpiarVisitados();
        int d = cp-pos[1];
        //Mover siempre hacia arriba si se puede.
        if(pos[0]>0 && pos[1]!=0 &&pos[1]!=7){
            pos[0]--;
        }
        else{
            pos[0]++;
        }
        if(d>0){
            if(pos[1]>0){
                pos[1]--;
            }
            else
                pos[1]++;
        }
        else{
            if(pos[1]<7){
                pos[1]++;
            }
            else
                pos[1]--;
        }
        pila.apilar("A("+pos[0]+","+pos[1]+") ");
    }
     /**
      * Este método movera el alfil cuando el método jugarAlfil() no encuentre ningúna solución. 
      * @param pos
      * @param cp
      * @param pila 
      */
    public void moverAlfilAux(int pos[],int cp, Pila<String> pila){
        if(this.dirPeon){
             moverAlfilAuxAbajo(pos, cp, pila);
        }
        else 
            moverAlfilAuxArriba(pos, cp, pila);
    }
    /**
     * Recibe la fila del PEON y verifica si el juego terminó. 
     * @param fila Fila del PEON
     * @return Si va hacia arriba: Returna TRUE cuando llegue a la fila 0.
     *         Si va hacia abajo : Retorna TRUE cuando fila==7
     */
    private boolean gameOver(int fila){
        if(this.dirPeon)
            return fila==7;
        else 
            return fila==0;
    }
    
    /**
     * Este método ejecuta todo el juego y returna los movimientos tanto del alfil como del PEON 
     * @return Retorna un String con los movimientos necesarios para acabar el juego.
     */
     public String jugar(){
        System.out.println("JUGAR");
        
        Pila<String> pilaA = new Pila<>();
        Pila <String> movimientos=new Pila<>();
        Pila<Posicion> pilaSol= new Pila<>();
        
        int arr[]=new int[4];
        recorrer(arr);
  
        buscarCruce(arr[0], arr[1], arr[3],arr[2]);
        imprimirTablero();
        int pos[]= new int [2];
        pos[0]=arr[0];
        pos[1]=arr[1];
        
        
        int juegoPeon = jugarPeon(arr[2],arr[3],movimientos);
        imprimirTablero();
        
        Posicion aux = new Posicion(pos[0], pos[1]);
        
        movimientos.apilar("A("+arr[0]+","+arr[1]+") ");
        
       
         while(!gameOver(juegoPeon)){
          
           arr[2]=juegoPeon;
           if( jugarAlfil(pilaSol,pos, arr[3], juegoPeon,pilaA).esVacia()){
               moverAlfilAux(pos, arr[3],movimientos);
               buscarCruce(pos[0], pos[1], arr[3],juegoPeon);
           }
            imprimirTablero();
            juegoPeon = jugarPeon(juegoPeon, arr[3],movimientos);
        }
        pilaSol.apilar(aux);
      
        marcarMovimientos(pilaSol);
        imprimirTablero();
          
        return mostrarMovimientos(movimientos,pilaA,arr[3]);
        
        
    }
     public static void main(String arg[]){
      
       
      Tablero t = new Tablero(1, 3, 0, 3, true);
         System.out.println("MOVIMIENTOS: "+ t.jugar() );
      
        t.imprimirTablero();
        
    }
}
