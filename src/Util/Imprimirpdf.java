/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import com.itextpdf.text.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 *  @author JEISON_FORT,GUILLERMO_GU
 */
public class Imprimirpdf {

    Image ficha;

    public Imprimirpdf(String url, String solucion, boolean direccion) {

        Document doc = new Document();

        String poses[] = solucion.split(" ");
        String nuevapo[] =this.transforma(poses);
        try {
            FileOutputStream ficheroPdf = new FileOutputStream(url);
            PdfWriter.getInstance(doc, ficheroPdf);
            doc.open();
            Font fuente = new Font();
            fuente.setSize(30);
            fuente.setColor(new BaseColor(118, 150, 86));

            Paragraph parrafo = new Paragraph();
            parrafo.setFont(fuente);
            doc.add(new Paragraph(" "));
            
            parrafo.add("                  ---Solución Alfil---");

            doc.add(parrafo);

            //Creando una tabla:
            int columnas = 8;
            Image imagen = Image.getInstance("src/Imagen/Vacia.png");

            for (int u = 0; u < poses.length; u++){
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));

                doc.add(new Paragraph("     - Tablero " + (u + 1)));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                PdfPTable tabla = new PdfPTable(columnas);

                for (int i = 0; i < 8; i++) {

                    for (int j = 0; j < 8; j++) {
                        PdfPCell celda = new PdfPCell(imagen);
                        for (int l = 0; l <= u; l++) {

                            if (i == Character.getNumericValue(poses[l].charAt(2))
                                    && j == Character.getNumericValue(poses[l].charAt(4))) {
                                this.desicionImagen(poses[l], direccion);
                                celda = new PdfPCell(ficha);

                            }

                        }

                        if ((i + j + 1) % 2 == 0) {

                            celda.setBackgroundColor(new BaseColor(118, 150, 86));

                        } else {
                            celda.setBackgroundColor(new BaseColor(238, 238, 210));

                        }
                        tabla.addCell(celda);

                    }

                }

                doc.add(tabla);
                
            
                String sol2 = "";
                for (int i = 0; i <= u; i++) {

                    sol2 += nuevapo[i]+ " ";
                }
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph(" "));

               
                doc.add(new Paragraph(" "));
                doc.add(new Paragraph("Movimiento : "+ sol2));
 
                doc.newPage();

            }
            doc.close();
            Pdf(doc);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    public void desicionImagen(String pos, boolean direccion) throws BadElementException, IOException {
        //true arfil negro sino pos blanco
        // peon blanco sino pos negro

        if (pos.charAt(0) == 'A' && direccion == true) {

            this.ficha = Image.getInstance("src/Imagen/ficha_negra1.png");

        } else if (pos.charAt(0) == 'A' && direccion == false) {

            this.ficha = Image.getInstance("src/Imagen/ficha_blanca2.png");

        } else if (pos.charAt(0) == 'P' && direccion == false) {

            this.ficha = Image.getInstance("src/Imagen/ficha_negra.png");

        } else {
            this.ficha = Image.getInstance("src/Imagen/ficha_blanca.png");
        }

    }

    public void Pdf(Document d) {

        try {
            File path = new File("src/pdf/Sol_Alfil.pdf");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String[] transforma(String poses[]) {

        
        int d = 0;
        int u = 0;
         String [] nuevapos= new String[poses.length];  
        
        System.arraycopy(poses, 0, nuevapos, 0, poses.length);
         
        
         for (int f = 0; f < poses.length; f++) {
            
                    
             
                    u = Character.getNumericValue(poses[f].charAt(2));
                    u++;
                    d = Character.getNumericValue(poses[f].charAt(4));
                    d++;
                    
                    nuevapos[f] = poses[f].charAt(0)+"("+u+","+d+") ";

                }

            
        
            
           
       
        return nuevapos;
    }

}
