/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinario.java,v 2.0 2020/12/14
 * Universidad Francisco de Paula Santander
 * Programa Ingenieria de Sistemas
 * Estructuras de datos
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;

/**
 * Implementacion de Clase para el manejo de un Arbol Binario.
 *
 * @param <T> Tipo de datos a almacenar en el Arbol Binario.
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinario<T> {

    ////////////////////////////////////////////////////////////
    // ArbolBinario - Atributos ////////////////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Nodo raiz del Arbol Binario
     */
    private NodoBin<T> raiz;

    ////////////////////////////////////////////////////////////
    // ArbolBinario - Implementacion de Metodos ////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Crea un Arbol Binario vacio. <br>
     * <b>post: </b> Se creo un Arbol Binario vacio.<br>
     */
    public ArbolBinario() {
        this.raiz = null;
    }

    /**
     * Crea un Arbol Binario con una raiz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Arbol con su raiz definida.<br>
     *
     * @param raiz Un objeto de tipo T que representa del dato en la raiz del
     * Arbol. <br>
     */
    public ArbolBinario(T raiz) {
        this.raiz = new NodoBin(raiz);
    }

    /**
     * Metodo que permite conocer el objeto de la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     *
     * @return la raiz del Arbol Binario.
     */
    public T getObjRaiz() {
        return (raiz.getInfo());
    }

    /**
     * Metodo que permite conocer la raiz del Arbol Binario. <br>
     * <b>post: </b> Se obtuvo la raiz del Arbol Binario.<br>
     *
     * @return la raiz del Arbol Binario.
     */
    public NodoBin<T> getRaiz() {
        return raiz;
    }

    /**
     * Obtiene la información de las hojas que están a la derecha, por ejemplo:
     * 5 / \ / \ 1 6 / \ 2 4 retornaría: 4
     *
     * @return un String con las hojas derechas
     */
    /**
     * Obtiene la información de las hojas (Decorador)
     *
     * @return un String con el info de las hojas
     */
    public String getInfoHojasDerecha() {
        return this.getInfoHojasDerechas(raiz);
    }

    public String getInfoHojasDerechas(NodoBin<T> r) {
        //Caso base: 
        if (r == null) {
            return "";
        }

        String info = "";
        if (r.getDer() != null && this.esHoja(r.getDer())) {
            info = r.getInfo().toString() + ",";
        }

        //Llamado a la recursión: 
        return (info + this.getInfoHojasDerechas(r.getIzq()) + this.getInfoHojasDerechas(r.getDer()));
    }

    public String getLukasiewicz() {
        return this.getLukasiewicz(raiz);
    }

    private String getLukasiewicz(NodoBin<T> r) {

        if (r == null) {
            return "b,";
        }
        return ("a," + this.getLukasiewicz(r.getIzq()) + this.getLukasiewicz(r.getDer()));
    }

    private String getPreOrden(NodoBin<T> r) {

        if (r == null) {
            return "";
        }
        return (r.getInfo().toString() + "," + this.getPreOrden(r.getIzq()) + "," + this.getPreOrden(r.getDer()));
    }

    private String getInOrden(NodoBin<T> r) {

        if (r == null) {
            return "";
        }
        return (this.getInOrden(r.getIzq()) + "," + r.getInfo().toString() + "," + this.getInOrden(r.getDer()));
    }

    private String getPostOrden(NodoBin<T> r) {

        if (r == null) {
            return "";
        }
        return (this.getPostOrden(r.getIzq()) + "," + this.getPostOrden(r.getDer()) + "," + r.getInfo().toString());
    }

    /**
     * Metodo que permite modificar la raiz del Arbol Binario. <br>
     * <b>post: </b> Se modifico la raiz del Arbol Binario.<br>
     *
     * @param raiz representa la nueva raiz del Arbol Binario.
     */
    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    /**
     * Obtiene la información de las hojas (Decorador)
     *
     * @return un String con el info de las hojas
     */
    public String getInfoNodosRamas() {

        return this.getInfoNodosRamas(raiz);
    }

    private String getInfoNodosRamas(NodoBin<T> r) {
        //Caso base: 
        if (r == null) {
            return "";
        }

        String info = "";
        if (!this.esHoja(r)) {
            info = r.getInfo().toString() + ",";
        }

        //Llamado a la recursión: 
        return (info + this.getInfoNodosRamas(r.getIzq()) + this.getInfoNodosRamas(r.getDer()));
    }

    /**
     * Obtiene la información de las hojas (Decorador)
     *
     * @return un String con el info de las hojas
     */
    public String getHojas() {

        return this.getHojas(raiz);
    }

    private String getHojas(NodoBin<T> r) {
        //Caso base: 
        if (r == null) {
            return "";
        }

        String info = "";
        if (this.esHoja(r)) {
            info = r.getInfo().toString() + ",";
        }

        //Llamado a la recursión: 
        return (info + this.getHojas(r.getIzq()) + this.getHojas(r.getDer()));
    }

    /**
     * Método que obtiene la cantidad de nodos hojas del árbol binario
     *
     * @return un entero con la cantidad de hojas
     */
    public int getCantidadHojas() {

        NodoBin<T> r = this.raiz;
        int cant = this.getCantidadHojas(r);
        return cant;  // this.getCantidadHojas(this.raiz);
    }

    /**
     * Método recursivo para contar hojas
     *
     * @param r de tipo NodoBIn y representa la raíz de cada subárbol
     * @return un entero con la cantidad de hojas
     */
    private int getCantidadHojas(NodoBin<T> r) {
        //Caso base: 
        if (r == null) {
            return 0;
        }
        int con = 0;
        if (this.esHoja(r)) {
            con = 1;
        }

        //Llamado a la recursión: 
        return (con + this.getCantidadHojas(r.getIzq()) + this.getCantidadHojas(r.getDer()));
    }

    private boolean esHoja(NodoBin<T> nodo) {

        return (nodo != null && nodo.getIzq() == null && nodo.getDer() == null);
    }

    /**
     * Metodo que permite mostrar por consola la informacion del Arbol Binario.
     * <br>
     *
     * @param n Representa la raiz del ArbolBinario o de alguno de sus
     * subarboles.
     */
    public void imprime(NodoBin<T> n) {
        T l = null;
        T r = null;
        if (n == null) {
            return;
        }
        if (n.getIzq() != null) {
            l = n.getIzq().getInfo();
        }
        if (n.getDer() != null) {
            r = n.getDer().getInfo();
        }
        System.out.println("NodoIzq: " + l + "\t Info: " + n.getInfo() + "\t NodoDer: " + r);
        if (n.getIzq() != null) {
            imprime(n.getIzq());
        }
        if (n.getDer() != null) {
            imprime(n.getDer());
        }
    }

    public boolean estaElemento(T info) {
        return busquedaElemento(info, this.raiz);
    }

    private boolean busquedaElemento(T info, NodoBin<T> n) {
        if (n == null) {
            return false;
        }
        if (n.getInfo().equals(info)) {
            return true;
        }
        return busquedaElemento(info, n.getIzq()) || this.busquedaElemento(info, n.getDer());
    }

    public String getElementos_Nivel() {
        String msg = "";
        NodoBin<T> r = this.raiz;

        if (r != null) {
            Cola<NodoBin<T>> direcciones = new Cola();
            direcciones.enColar(r);
            while (!direcciones.esVacia()) {
                r = direcciones.deColar();
                msg += r.getInfo().toString() + ",";
                if (r.getIzq() != null) {
                    direcciones.enColar(r.getIzq());
                }
                if (r.getDer() != null) {
                    direcciones.enColar(r.getDer());
                }
            }

        }

        return msg;
    }
    /**
     * Metodo de parcial final: Encontrar las info de las hojas presentes en un nivel n
     * @param n
     * @return 
     */
    public String getHojasNivel(int n) {
        if(this.raiz==null)
            return "Arbol vacio";
        String msg =getHojasNivel(n, 0, raiz);
        if(msg.equals(""))
            return "No se encontró ninguna hoja";
       return msg;
    }
    /**
     * Método que realiza la busqueda de getHojasNivel
     * @param b
     * @param i
     * @param n
     * @return 
     */
    private String getHojasNivel(int b,int i, NodoBin<T> n){
        
        if(n==null)
            return "";
        if(i==b){
            if(this.esHoja(n)){
               i--;
               return n.getInfo().toString()+", ";
            }
        }
        else if(i+1>b){
            i--;
            return "";
        }
            
        i++;
        return getHojasNivel(b, i, n.getIzq())+getHojasNivel(b, i, n.getDer());
    }

    public String getElementosNivelS() {
        if (this.raiz == null) {
            return "Arbol vacio";
        }
        String msg = "";
        NodoBin<T> r = this.raiz;
        int i = 2;
        msg = "N1:" + r.getInfo().toString() + " ";

        Cola<NodoBin<T>> direcciones = new Cola();
        direcciones.enColar(r);

        while (!direcciones.esVacia()) {
            msg = "N" + i + ":";
            r = direcciones.deColar();
            msg += r.getInfo().toString() + ",";
            if (r.getIzq() != null) {
                direcciones.enColar(r.getIzq());
            }
            if (r.getDer() != null) {
                direcciones.enColar(r.getDer());
            }
        }

        return msg;
    }

    /**
     * Retorna la descripción del elemento dado los siguiente puntos: 1. Info es
     * raíz 2. Info es padre izquierdo o derecho 3. Info es hijo izquierdo o
     * derecho 4. Info no existe en el árbol 5. El árbol es vacíom por lo tanto
     * info no existe
     *
     * Suponga que info SÓLO EXISTE UNA VEZ EN EL ÁRBOL.
     *
     * @param info el elemento a buscar en el árbol
     * @return un String con la descripción del elemento
     */
    public String getDescripcionElemento(T info) {
        // :)
        if (this.raiz == null) {
            return "El árbol es vacío por lo tanto info no existe";
        }
        if (this.raiz.getInfo().equals(info)) {
            return "Info es raíz";
        }
        Pila<NodoBin<T>> pila = new Pila();
        String resp = this.getDescripcionElemento(info, this.raiz, pila);
        return (resp.equals("")) ? "Info no existe en el árbol" : resp;
    }

    /**
     * Retorna la descripción del elemento dado los siguiente puntos: 1. Info es
     * raíz 2. Info es padre izquierdo o derecho 3. Info es hijo izquierdo o
     * derecho 4. Info no existe en el árbol 5. El árbol es vacíom por lo tanto
     * info no existe
     *
     * Suponga que info SÓLO EXISTE UNA VEZ EN EL ÁRBOL.
     *
     * @param info el elemento a buscar en el árbol
     * @return un String con la descripción del elemento
     */

    private String getDescripcionElemento(T info, NodoBin<T> n, Pila<NodoBin<T>> pila) {
        // :)
        if (n == null) {
            return "";
        }
        if (n.getInfo().equals(info)) {
            if (this.esHoja(n)) {
                if (!pila.esVacia() && pila.getTope().getIzq().equals(n)) {
                    return "Info es hoja izquierdo";
                } else {
                    return "Info es hoja derecho";
                }
            } else {
                if (!pila.esVacia() && pila.getTope().getIzq().equals(n)) {
                    return "Info es padre izquierdo";
                } else {
                    return "Info es padre derecho";
                }
            }
        } else {
            if (!this.esHoja(n)) {
                pila.apilar(n);
            }
            return getDescripcionElemento(info, n.getIzq(), pila) + getDescripcionElemento(info, n.getDer(), pila);
        }
    }

    public String getDescripcionElementoD(T info) {
        String salida = dondeSeEncuentra(info, this.raiz);
        if (salida.equals("")) {
            return "Info no existe en el árbol";
        }
        return salida;
    }

    private String dondeSeEncuentra(T info, NodoBin<T> r) {
        if (r == null) {
            return "El árbol es vacío por lo tanto info no existe";
        }
        if (r.getInfo().equals(info)) {
            if (r.equals(this.raiz)) {
                return "Info es raíz";
            }
            if (r.getDer() != null && r.getIzq() != null) {
                return "Info es padre izquierdo o derecho";
            } else {
                return "Info es hoja izquierdo o derecho";
            }
        }
        if (r.getDer() == null && r.getIzq() == null) {
            return "";
        }

        return "" + dondeSeEncuentra(info, r.getDer()) + dondeSeEncuentra(info, r.getIzq());
    }
}//Fin de la Clase ArbolBinario
